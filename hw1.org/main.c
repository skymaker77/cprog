#include <stdio.h>





int main(void)
{
    int n; // 삼각형 길이 (가로,세로)
    scanf("%d",&n); // 세로

    printf("Hello world\n");
    // 삼각형 거꾸로
    int k;
    for (k = n; k > 0; --k)
    {
        int i;
        for (i = 0; i < k; ++i)
        {
            printf("*");
        }
        printf("\n");
    }

    // 삼각형
    int j;
    for (j = 0; j < n; ++j)
    {
        int i;
        for (i = 0; i <= j; ++i)
        {
            printf("*");
        }
        printf("\n");
    }
    printf("Bye world\n");

    return 0;
}

/*
int main()
{
    int m; // 가로
    int n; // 세로
    // p.275 에서 6번 문제 (사각형)

    scanf("%d",&m); // 가로
    scanf("%d",&n); // 세로

    printf("Hello world\n");
    // 가로 한줄 찍기
    int k;
    for (k = 0; k < n; ++k)
    {
        int i;
        for (i = 0; i < m; ++i)
        {
            printf("*");
        }
        printf("\n");
    }
    printf("Bye world\n");
    return 0;
}
*/
