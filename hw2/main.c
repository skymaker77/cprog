#include <stdio.h>

int main(void)
{
   char str1[128], str2[128];

   scanf("%s", str1);

   int n=0;
   while(str1[n]!='\0') n++;

   int i=0, j=0;
   for(i=n-1; i>=0; --i)
   {
        printf("%c", str1[i]);
        str2[j++]=str1[i];
   }

   printf("\n");

   int k=0;
   for(i=0; i<n; i++)
   {
       if(str1[i]==str2[i]) k++;
   }

   if(k==n)
   {
       printf("palindrome\n");
   }
}
