#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int wordcount = 0; // 총 단어 개수
    int ulwdcount = 0; // 대문자로 시작해 소문자로 끝나는 단어 개수

    int start = 1;
    int upper = 0;

    char c=NULL, prev_c=NULL;

    while (EOF !=( c = getchar()))
    {
        if( isspace(c) )
        {
            start = 1;
            if( upper && islower(prev_c) )
            {
                ++ulwdcount;
                upper = 0;
            }
        }
        else
        {
            if(start == 1)
            {
                ++wordcount;
                if( isupper(c) ) upper=1;
                start = 0;
            }
            else
            {
                prev_c = c;
            }
         }
    }

    printf("%d\n", wordcount); // 전체 단어 개수
    printf("%d\n", ulwdcount); // 첫글자 대문자이고 마지막 글자 소문자인 단어 개수

    return 0;
}
